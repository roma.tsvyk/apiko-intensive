import React from 'react';
import { Link, useLocation } from 'react-router-dom';

import { routes } from './routes';

const modifiedAboutRoute = `${routes.ABOUT}?name=test About Name`;

export const Header = () => {
  const { pathname } = useLocation();
  return pathname === routes.LOGIN ? null : (
    <ul>
      <li>
        <Link to={routes.HOME}>Home</Link>
      </li>
      <li>
        <Link to={routes.PRODUCTS}>Products</Link>
      </li>
      <li>
        <Link to={modifiedAboutRoute}>About</Link>
      </li>
    </ul>
  );
};
