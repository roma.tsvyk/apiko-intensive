import React, { Fragment} from 'react';
import { useLocation } from 'react-router-dom';

export const AboutPage = () => {
  const { search } = useLocation();
  const name = 'name';
  const value = new URLSearchParams(search).get(name);

  return (
    <Fragment>
      <div>About Page</div>
      <div>The query string name is {name} and value is {value}</div>
    </Fragment>
  );
};