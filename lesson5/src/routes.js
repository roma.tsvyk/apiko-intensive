export const routes = {
  HOME: '/',
  PRODUCTS: '/products',
  ABOUT: '/about',
  NOT_FOUND: '/not-found',
  LOGIN: '/login',
};
