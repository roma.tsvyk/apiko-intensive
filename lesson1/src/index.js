import React, { Fragment } from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import { Clock } from './clock';

class Body extends React.Component {
  render() {
    return (
      <Fragment>
        {this.props.todoList.map(todo => <div key={todo}>{todo}</div>)}
      </Fragment>
    );
  }
}

Body.defaultProps = { todoList: [] };
Body.displayName = 'TodoList';

class App extends React.Component {
  state = { todos: ['first', 'second', 'third'] };

  onAdd = todo => this.setState({ todos: [...this.state.todos, todo] });

  render() {
    return (
      <div className="app">
        <Clock />
      </div>
    )
  }
}

ReactDOM.render(<App/>, document.getElementById('root'));
