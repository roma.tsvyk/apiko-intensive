import React from 'react';

import { Header } from './header';

export class Clock extends React.Component {
  state = { time: new Date(), test: '' };

  tick = () => {
    this.setState({ time: new Date() });
    console.log('Tick');
  };

  componentDidMount() {
    this.timer = setInterval(() => this.tick(), 1000);
  }

  onAdd = () => console.log('Added');

  render() {
    return (
      <div>
        <Header onAdd={this.onAdd}/>
        <div>Time: {this.state.time.toLocaleTimeString()}</div>
      </div>
    );
  }
}