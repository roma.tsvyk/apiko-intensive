import { useReducer, useEffect } from 'react';

import { initialState, TODOS_ACTIONS, todosReducer } from './todosReducer';

export const useTodosHook = () => {
  const [todos, dispatch] = useReducer(todosReducer, initialState());

  const onAdd = text => dispatch({
    text,
    type: TODOS_ACTIONS.ADD,
  });

  const onSwitch = _id => dispatch({
    _id,
    type: TODOS_ACTIONS.COMPLETE,
  });

  const onRemove = _id => dispatch({
    _id,
    type: TODOS_ACTIONS.REMOVE,
  });

  const onEdit = newTodo => dispatch({
    newTodo,
    type: TODOS_ACTIONS.EDIT,
  });

  useEffect(() => {
    const todosStringified = JSON.stringify(todos);
    localStorage.setItem('todos', todosStringified);
  }, [todos]);

  return {
    todos,
    onAdd,
    onEdit,
    onSwitch,
    onRemove,
  };
};
