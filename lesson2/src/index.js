import React, { useState } from 'react';
import ReactDOM from 'react-dom';

import { Header } from './header';
import { Clock } from './clock';
import { Body } from './body';
import './index.css';

const App = () => {
  const [app, setApp] = useState({
    todos: ['first', 'second', 'third'],
    showClock: true,
  });

  const onAdd = todo => setApp({ ...app, todos: [...app.todos, todo] });

  const onSwitch = () => setApp({ ...app, showClock: !app.showClock });

  const { todos, showClock} = app;

  return (
    <div className="app">
      <Header onAdd={onAdd}>ToDo List Header</Header>
      <Body todoList={todos} />
      <button onClick={onSwitch} className="clockBtn">
        Turn Clock {app.showClock ? 'Off' : 'On'}
      </button>
      {showClock && <Clock onSwitch={onSwitch} />}
    </div>
  );
};

ReactDOM.render(<App/>, document.getElementById('root'));
