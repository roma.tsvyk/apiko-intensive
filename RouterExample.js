import React from 'react';
import { Switch, Redirect, Route, withRouter } from 'react-router-dom';

export const routesNames = {
  HOME: '/',
  NOT_FOUND: '*',
  LOGIN: '/login',
};

// add here routes which not redirect to Login Page
export const whiteList = [routesNames.LOGIN];

const routesConfig = [
  [routesNames.LOGIN, () => <h1>Login</h1>],
  [routesNames.HOME, () => <h1>Home</h1>, true],
  [routesNames.NOT_FOUND, () => <h1>Not Found</h1>],
];

const MainLayout = () => <div>Main Layout</div>;

const AuthLayout = () => <div>Auth Layout</div>;

const isNotFromWhiteList = path =>
  whiteList.find(it => it === path) === undefined;

const shouldRedirectRoute = (authorized, path) =>
  !authorized && isNotFromWhiteList(path);

const pickRouteLayout = route =>
  isNotFromWhiteList(route) ? MainLayout : AuthLayout;

const AppRoutes = ({ authorized, location }) => {
  // get it from localstorage or from server with token to know if we are already logged in
  const isAuthorized = authorized || false;
  const Layout = pickRouteLayout(location.pathname);

  return (
    <Layout>
      <Switch>
        {routesConfig.map(
          ([routeName, component, exact]) => shouldRedirectRoute(isAuthorized, routeName)
            ? <Redirect key={routeName} from={routeName} to={routesNames.LOGIN} />
            : <Route key={routeName} path={routeName} {...{ component, exact }} />
        )}
      </Switch>
    </Layout>
  );
};

// you can use Router Hooks instead of withRouter HOC
export default withRouter(AppRoutes);
